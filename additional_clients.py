from dbt.cli.main import dbtRunner, dbtRunnerResult

import os

os.environ["DBT_ENV_SECRET_HOST"] = ""
os.environ["DBT_ENV_DB_PORT"] = ""
os.environ["DBT_ENV_USER"] = ""
os.environ["DBT_ENV_SECRET_PASSWORD"] = ""
os.environ["DBT_ENV_DB_NAME"] = ""

def run_additional_client_1():
    # initialize
    dbt = dbtRunner()
    
    # create CLI args as a list of strings
    cli_args = ["run"]
    
    # Create your custom variables 
    dbt_run_variables = {
        'key': 'value'
    }

    # Set the schema that the model will run in
    os.environ["DBT_ENV_SCHEMA"] = "test"
    
    # run the command
    res: dbtRunnerResult = dbt.invoke(cli_args, vars=dbt_run_variables)
    
    # inspect the results
    for r in res.result:
        print(f"{r.node.name}: {r.status}")


def run_additional_client_2():
    # initialize
    dbt = dbtRunner()
    
    # create CLI args as a list of strings
    cli_args = ["run"]
    
    # Create your custom variables 
    dbt_run_variables = {
        'key': 'value'
    }
    
    # Set the schema that the model will run in
    os.environ["DBT_ENV_SCHEMA"] = "test"

    # run the command
    res: dbtRunnerResult = dbt.invoke(cli_args, vars=dbt_run_variables)
    
    # inspect the results
    for r in res.result:
        print(f"{r.node.name}: {r.status}")


def run_additional_client_3():
    # initialize
    dbt = dbtRunner()
    
    # create CLI args as a list of strings
    cli_args = ["run"]
    
    # Create your custom variables 
    dbt_run_variables = {
        'key': 'value'
    }
    
    # Set the schema that the model will run in
    os.environ["DBT_ENV_SCHEMA"] = "test"

    # run the command
    res: dbtRunnerResult = dbt.invoke(cli_args, vars=dbt_run_variables)
    
    # inspect the results
    for r in res.result:
        print(f"{r.node.name}: {r.status}")



if __name__ == '__main__':
    run_additional_client_1()
    run_additional_client_2()
    run_additional_client_3()