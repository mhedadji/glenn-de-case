# nodus-de-case
In this repository, you will find the necessary structure to create your DBT models and run them with custom variables via Python. As you develop your models, you should test them using the `sample_client.py` file. Once you have finished the models, you should run them against the additional clients using the `additional_clients.py` file.


## Getting Started

1. Install Python requirements: `pip install -r requirements.txt`
    - It is highly encouraged to use a Python virtual environment to manage your dependencies. 
2. Update the database connection variables at the top of the `sample_client.py` file. 
    - The connection information for your database is provided in the `db_connection` file. 
3. Run the sample DBT project using the sample client python file: `python sample_client.py`


## Delivery
1. Once you have finished, you should push your code to a branch called `final`. 
